package com.gankao.fmdemo;

import android.app.Application;

import com.gankao.gkfm.GKFMConfig;

public class App extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        GKFMConfig.init(this, "debug", "12345678", "debug666888");
    }
}
