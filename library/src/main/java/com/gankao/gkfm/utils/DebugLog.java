package com.gankao.gkfm.utils;

import android.util.Log;

import com.gankao.gkfm.BuildConfig;

public class DebugLog {
    private static final String TAG = "test";

    public static void e(CharSequence msg) {
        if (BuildConfig.DEBUG) {
            Log.e(TAG, "" + msg);
        }
    }

    public static void e(boolean msg) {
        if (BuildConfig.DEBUG) {
            Log.e(TAG, "" + msg);
        }
    }

    public static void e(int msg) {
        if (BuildConfig.DEBUG) {
            Log.e(TAG, "" + msg);
        }
    }
}
