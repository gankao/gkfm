package com.gankao.gkfm.utils;

import java.util.Calendar;
import java.util.Date;

public class DateUtils {
    public static String getDate() {
        return getDate(new Date());
    }

    public static String getDate(long millseconds) {
        return getDate(new Date(millseconds));
    }

    public static String getDate(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        //1.常用方法：get(int field)
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH) + 1;
        int day = c.get(Calendar.DAY_OF_MONTH);
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);
        int second = c.get(Calendar.SECOND);
        int week = c.get(Calendar.DAY_OF_WEEK);//1--7的值,对应：星期日，星期一，星期二，星期三....星期六
        String weekStr = getWeek(week);
        String dateStr = year + "年" + month + "月" + day + "日 (" + weekStr + ") " + (hour < 10 ? "0" + hour : hour) + ":" + (minute < 10 ? "0" + minute : minute);
        return dateStr;
    }

    private static String getWeek(int week) {
        String[] arr = {"星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"};
        return arr[week - 1];
    }
}
