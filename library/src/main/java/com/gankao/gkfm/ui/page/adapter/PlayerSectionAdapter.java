package com.gankao.gkfm.ui.page.adapter;

import android.content.Context;
import android.os.Debug;
import android.view.ViewGroup;

import androidx.core.content.ContextCompat;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import com.dragonpower.architecture.binding_recyclerview.adapter.SimpleDataBindingAdapter;
import com.dragonpower.architecture.utils.DisplayUtils;
import com.gankao.gkfm.R;
import com.gankao.gkfm.data.bean.Section;
import com.gankao.gkfm.databinding.AdapterPlayerSectionBinding;
import com.gankao.gkfm.databinding.AdapterSectionBinding;
import com.gankao.gkfm.utils.DebugLog;

public class PlayerSectionAdapter extends SimpleDataBindingAdapter<Section, AdapterPlayerSectionBinding> {
    public MutableLiveData<Integer> currentPosition;

    public PlayerSectionAdapter(Context context, MutableLiveData<Integer> currentPosition) {
        super(context, R.layout.adapter_player_section, DiffUtils.getInstance().getSectionItemCallback());
        this.currentPosition = currentPosition;
    }

    @Override
    protected void onBindItem(AdapterPlayerSectionBinding binding, Section item, RecyclerView.ViewHolder holder) {
        binding.setVm(item);
        DebugLog.e(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> " + holder.getAbsoluteAdapterPosition());
        binding.tvTitle.setTextColor(ContextCompat.getColor(binding.getRoot().getContext(), holder.getAbsoluteAdapterPosition() == currentPosition.getValue()
                ? R.color.light_blue : R.color.white));
        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) binding.ivImage.getLayoutParams();
        layoutParams.rightMargin = (holder.getAbsoluteAdapterPosition() == getItemCount() - 1) ? DisplayUtils.dp2px(24) : DisplayUtils.dp2px(12);
        layoutParams.leftMargin = (holder.getAbsoluteAdapterPosition() == 0) ? DisplayUtils.dp2px(24) : 0;
        binding.ivImage.setLayoutParams(layoutParams);
    }
}
