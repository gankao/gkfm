package com.gankao.gkfm.ui.state;


import androidx.databinding.ObservableBoolean;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.gankao.gkfm.data.bean.PlayerSpeed;
import com.gankao.gkfm.data.bean.PlayerTimeOff;
import com.gankao.gkfm.data.bean.Section;
import com.gankao.gkfm.domain.request.PlayerRequest;

import java.util.List;

public class PlayerViewModel extends ViewModel {
    private LifecycleOwner lifecycleOwner;

    public void setLifecycleOwner(LifecycleOwner lifecycleOwner) {
        this.lifecycleOwner = lifecycleOwner;
    }

    public final MutableLiveData<List<PlayerTimeOff>> timeOffList = new MutableLiveData<>();
    public final MutableLiveData<Boolean> notifyTimeOffDataSetChanged = new MutableLiveData();

    public final MutableLiveData<List<Section>> sectionList = new MutableLiveData<>();
    public final MutableLiveData<Boolean> notifySectionDataSetChanged = new MutableLiveData();

    public final MutableLiveData<List<PlayerSpeed>> speedList = new MutableLiveData<>();
    public final MutableLiveData<Boolean> notifySpeedDataSetChanged = new MutableLiveData();

    public final MutableLiveData<Integer> currentTimeOffPosition = new MutableLiveData<>();
    public final MutableLiveData<Integer> currentSpeedPosition = new MutableLiveData<>();
    public final MutableLiveData<Integer> currentSectionPosition = new MutableLiveData<>();
    public final MutableLiveData<PlayerTimeOff> currentTimingOff = new MutableLiveData<>();
    public final MutableLiveData<Section> currentSection = new MutableLiveData<>();
    public final MutableLiveData<PlayerSpeed> currentSpeed = new MutableLiveData<>();

    public final MutableLiveData<Boolean> showTimeOff = new MutableLiveData();
    public final MutableLiveData<Boolean> showSpeed = new MutableLiveData();
    public final MutableLiveData<Boolean> showSection = new MutableLiveData();

    public final MutableLiveData<String> currentSectionTime = new MutableLiveData<>();
    public final MutableLiveData<String> totalSectionTime = new MutableLiveData<>();
    public final MutableLiveData<Integer> progress = new MutableLiveData<>();
    public final MutableLiveData<Integer> max = new MutableLiveData<>();
    public final ObservableBoolean isPlaying = new ObservableBoolean();

    public final MutableLiveData<Boolean> order = new MutableLiveData();

    public final PlayerRequest request = new PlayerRequest();

    {
        currentTimeOffPosition.setValue(0);
        currentSpeedPosition.setValue(1);
        currentSectionPosition.setValue(0);
        showTimeOff.setValue(false);
        showSpeed.setValue(false);
        showSection.setValue(false);
        currentSectionTime.setValue("00:00");
        totalSectionTime.setValue("00:00");
        order.setValue(false);
    }
}
