package com.gankao.gkfm.ui.page.adapter;

import android.content.Context;

import androidx.core.content.ContextCompat;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import com.dragonpower.architecture.binding_recyclerview.adapter.SimpleDataBindingAdapter;
import com.dragonpower.architecture.utils.DisplayUtils;
import com.gankao.gkfm.R;
import com.gankao.gkfm.data.bean.Section;
import com.gankao.gkfm.databinding.AdapterSectionBinding;
import com.gankao.gkfm.utils.DebugLog;

public class SectionAdapter extends SimpleDataBindingAdapter<Section, AdapterSectionBinding> {
    public MutableLiveData<Integer> currentPosition;

    public SectionAdapter(Context context, MutableLiveData<Integer> currentPosition) {
        super(context, R.layout.adapter_section, DiffUtils.getInstance().getSectionItemCallback());
        this.currentPosition = currentPosition;
    }

    @Override
    protected void onBindItem(AdapterSectionBinding binding, Section item, RecyclerView.ViewHolder holder) {
        binding.setVm(item);
        DebugLog.e(holder.getAbsoluteAdapterPosition() + "," + (getItemCount() - 1));
        if (holder.getAbsoluteAdapterPosition() == 0) {
            binding.rootView.setBackgroundResource(R.mipmap.fm_recycleview_item_bg_left);
            binding.rootView.setPadding(DisplayUtils.dp2px(36), 0, 0, 0);
        }
        else if (holder.getAbsoluteAdapterPosition() == getItemCount() - 1) {
            binding.rootView.setBackgroundResource(R.mipmap.fm_recycleview_item_bg_right);
            binding.rootView.setPadding(0, 0, DisplayUtils.dp2px(12), 0);
        }
        else {
            binding.rootView.setBackgroundResource(R.mipmap.fm_recycleview_item_bg_center);
            binding.rootView.setPadding(0, 0, 0, 0);
        }
    }
}
