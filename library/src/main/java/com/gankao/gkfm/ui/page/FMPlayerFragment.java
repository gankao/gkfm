package com.gankao.gkfm.ui.page;

import android.os.Bundle;
import android.os.Debug;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.SeekBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.dragonpower.architecture.http.EasyConfig;
import com.dragonpower.architecture.ui.page.BaseFragment;
import com.dragonpower.architecture.ui.page.DataBindingConfig;
import com.gankao.gkfm.BR;
import com.gankao.gkfm.R;
import com.gankao.gkfm.data.bean.Anchor;
import com.gankao.gkfm.data.bean.PlayerTimeOff;
import com.gankao.gkfm.data.bean.Section;
import com.gankao.gkfm.data.bean.UserInfo;
import com.gankao.gkfm.data.config.Configs;
import com.gankao.gkfm.player.PlayerManager;
import com.gankao.gkfm.ui.helper.DefaultInterface;
import com.gankao.gkfm.ui.page.adapter.AnchorAdapter;
import com.gankao.gkfm.ui.page.adapter.PlayerSectionAdapter;
import com.gankao.gkfm.ui.page.adapter.PlayerSpeedAdapter;
import com.gankao.gkfm.ui.page.adapter.SectionAdapter;
import com.gankao.gkfm.ui.page.adapter.TimeOffAdapter;
import com.gankao.gkfm.ui.state.FMMainViewModel;
import com.gankao.gkfm.ui.state.PlayerViewModel;
import com.gankao.gkfm.ui.state.SplashViewModel;
import com.gankao.gkfm.utils.DebugLog;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.gankao.gkfm.player.PlayerManager.STATE_PAUSE;

public class FMPlayerFragment extends BaseFragment {

    public FMPlayerFragment() {

    }

    private PlayerViewModel mViewModel;
    private FMMainViewModel mDictationViewModel;
    private TimeOffAdapter mTimeOffAdapter;
    private PlayerSpeedAdapter mPlayerSpeedAdapter;
    private PlayerSectionAdapter mSectionAdapter;
    private PlayerManager mPlayerManager;

    @Override
    protected void initViewModel() {
        mPlayerManager = new PlayerManager();
        mPlayerManager.init(getContext());
        mViewModel = getFragmentScopeViewModel(PlayerViewModel.class);
        mDictationViewModel = getActivityScopeViewModel(FMMainViewModel.class);
        mTimeOffAdapter = new TimeOffAdapter(getContext(), mViewModel.currentTimeOffPosition);
        mTimeOffAdapter.setOnItemClickListener((viewId, item, position) -> {
            mViewModel.currentTimeOffPosition.setValue(position);
            mViewModel.showTimeOff.setValue(false);
        });
        mPlayerSpeedAdapter = new PlayerSpeedAdapter(getContext(), mViewModel.currentSpeedPosition);
        mPlayerSpeedAdapter.setOnItemClickListener((viewId, item, position) -> {
            mViewModel.currentSpeedPosition.setValue(position);
            mViewModel.showSpeed.setValue(false);
        });
        mSectionAdapter = new PlayerSectionAdapter(getContext(), mViewModel.currentSectionPosition);
        mSectionAdapter.setOnItemClickListener((viewId, item, position) -> {
            mViewModel.currentSectionPosition.setValue(position);
            mViewModel.currentSection.setValue(item);
            mPlayerManager.playAtPosition(item);
        });
    }

    @Override
    protected DataBindingConfig getDataBindingConfig() {
        return new DataBindingConfig(R.layout.fragment_fm_player, BR.vm, mViewModel)
                .addBindingParam(BR.click, new ClickProxy())
                .addBindingParam(BR.event, new EventHandler())
                .addBindingParam(BR.adapter_timeoff, mTimeOffAdapter)
                .addBindingParam(BR.adapter_speed, mPlayerSpeedAdapter)
                .addBindingParam(BR.adapter_section, mSectionAdapter);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getArguments() != null) {
            Section section = (Section) getArguments().getSerializable(Configs.PLAY_BUNDLE);
            if (section != null) {
                mViewModel.currentSection.setValue(section);
            }
        }
        mViewModel.setLifecycleOwner(getViewLifecycleOwner());
        mViewModel.request.getTimingOffLiveData().observe(getViewLifecycleOwner(), dataResult -> {
            if (dataResult.getResponseStatus().isSuccess()) {
                mViewModel.timeOffList.setValue(dataResult.getResult());
                mViewModel.currentTimeOffPosition.setValue(0);
            }
        });
        mViewModel.request.getSpeedLiveData().observe(getViewLifecycleOwner(), dataResult -> {
            if (dataResult.getResponseStatus().isSuccess()) {
                mViewModel.speedList.setValue(dataResult.getResult());
                mViewModel.currentSpeedPosition.setValue(1);
            }
        });
        mViewModel.request.getGrowCourseLiveData().observe(getViewLifecycleOwner(), dataResult -> {
            if (dataResult.getResponseStatus().isSuccess()) {
                mViewModel.currentSection.setValue(dataResult.getResult().course);
                mViewModel.sectionList.setValue(dataResult.getResult().fmSections);
                for (int i = 0; i < dataResult.getResult().fmSections.size(); i++) {
                    if (dataResult.getResult().fmSections.get(i).id == dataResult.getResult().course.id) {
                        mViewModel.currentSectionPosition.setValue(i);
                    }
                }
                mPlayerManager.setAudioUrl(mViewModel.sectionList.getValue().get(mViewModel.currentSectionPosition.getValue()));
            }
        });
        mViewModel.currentTimeOffPosition.observe(getViewLifecycleOwner(), position -> {
            mViewModel.notifyTimeOffDataSetChanged.setValue(true);
            if (mViewModel.timeOffList.getValue() != null && mViewModel.timeOffList.getValue().size() > 0) {
                mViewModel.currentTimingOff.setValue(mViewModel.timeOffList.getValue().get(position));
            }
        });
        mViewModel.currentSpeedPosition.observe(getViewLifecycleOwner(), position -> {
            mViewModel.notifySpeedDataSetChanged.setValue(true);
            if (mViewModel.speedList.getValue() != null && mViewModel.speedList.getValue().size() > 0) {
                mViewModel.currentSpeed.setValue(mViewModel.speedList.getValue().get(position));
            }
        });
        mViewModel.currentSectionPosition.observe(getViewLifecycleOwner(), position -> {
            mViewModel.notifySectionDataSetChanged.setValue(true);
        });
        mViewModel.currentSpeed.observe(getViewLifecycleOwner(), playerSpeed -> {
            mPlayerManager.setSpeed(playerSpeed.value);
        });
        mViewModel.currentTimingOff.observe(getViewLifecycleOwner(), playerTimeOff -> {
            interval_handler.removeCallbacks(interval_Thread);
            if (playerTimeOff.value > 0) {
                interval_handler.postDelayed(interval_Thread, playerTimeOff.value * 60 * 1000);
            }
        });
        mViewModel.showSection.observe(getViewLifecycleOwner(), show -> {
            if (show) {
                mViewModel.currentSectionPosition.setValue(mViewModel.currentSectionPosition.getValue());
            }
        });

        mPlayerManager.STATE.observe(getViewLifecycleOwner(), state -> {
            mViewModel.isPlaying.set(state == PlayerManager.STATE_PLAYING || state == PlayerManager.STATE_INTELVAL_PLAYING);
            if (state == PlayerManager.STATE_COMPLETE) {
                if (mViewModel.currentTimingOff.getValue() != null && mViewModel.currentTimingOff.getValue().value == 0) {
                    nav().navigateUp();
                }
            }
        });
        mPlayerManager.getTotalDuration().observe(getViewLifecycleOwner(), duration -> {
            mViewModel.max.setValue(duration);
        });
        mPlayerManager.getCurrentProgress().observe(getViewLifecycleOwner(), progress -> {
            mViewModel.progress.setValue(progress);
        });
        mPlayerManager.getCurrentLetterTime().observe(getViewLifecycleOwner(), time -> {
            mViewModel.currentSectionTime.setValue(time);
        });
        mPlayerManager.getTotalTime().observe(getViewLifecycleOwner(), time -> {
            mViewModel.totalSectionTime.setValue(time);
        });
    }

    public void getTimeOffList() {
        if (mViewModel.timeOffList.getValue() == null || mViewModel.timeOffList.getValue().size() == 0) {
            mViewModel.request.getTimeOffList(getViewLifecycleOwner());
        }
    }

    public void getSpeedList() {
        if (mViewModel.speedList.getValue() == null || mViewModel.speedList.getValue().size() == 0) {
            mViewModel.request.getSpeedList(getViewLifecycleOwner());
        }
    }

    public void getSections() {
        if (mViewModel.sectionList.getValue() == null || mViewModel.sectionList.getValue().size() == 0) {
            mViewModel.request.getGrowCourse(getViewLifecycleOwner(), mViewModel.currentSection.getValue());
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getTimeOffList();
        getSpeedList();
        getSections();
        if (mPlayerManager.STATE.getValue() == STATE_PAUSE){
            mPlayerManager.togglePlay();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        DebugLog.e("onPause");
        mPlayerManager.pause();
    }


    public class ClickProxy {

        public void back() {
            nav().navigateUp();
        }

        public void closeAll() {
            mViewModel.showTimeOff.setValue(false);
            mViewModel.showSpeed.setValue(false);
            mViewModel.showSection.setValue(false);
        }

        public void timeoff() {
            mViewModel.showTimeOff.setValue(!mViewModel.showTimeOff.getValue());
            mViewModel.showSpeed.setValue(false);
            mViewModel.showSection.setValue(false);
        }

        public void speed() {
            mViewModel.showTimeOff.setValue(false);
            mViewModel.showSpeed.setValue(!mViewModel.showSpeed.getValue());
            mViewModel.showSection.setValue(false);
        }

        public void introduction() {
            mViewModel.showTimeOff.setValue(false);
            mViewModel.showSpeed.setValue(false);
            mViewModel.showSection.setValue(false);
        }

        public void section() {
            mViewModel.showTimeOff.setValue(false);
            mViewModel.showSpeed.setValue(false);
            mViewModel.showSection.setValue(!mViewModel.showSection.getValue());
        }

        public void togglePlay() {
            closeAll();
            mPlayerManager.togglePlay();
        }

        public void next() {
            closeAll();
            int position = mViewModel.currentSectionPosition.getValue();
            int targetPosition = position + 1;
            targetPosition = targetPosition >= mViewModel.sectionList.getValue().size() ? 0 : targetPosition;
            mPlayerManager.playNext(mViewModel.sectionList.getValue().get(targetPosition));
            mViewModel.currentSection.setValue(mViewModel.sectionList.getValue().get(targetPosition));
            mViewModel.currentSectionPosition.setValue(targetPosition);
        }

        public void preious() {
            closeAll();
            int position = mViewModel.currentSectionPosition.getValue();
            int targetPosition = position - 1;
            targetPosition = targetPosition < 0 ? mViewModel.sectionList.getValue().size() - 1 : targetPosition;
            mPlayerManager.playPreious(mViewModel.sectionList.getValue().get(targetPosition));
            mViewModel.currentSection.setValue(mViewModel.sectionList.getValue().get(targetPosition));
            mViewModel.currentSectionPosition.setValue(targetPosition);
        }

        public void sort() {
            mViewModel.order.setValue(!mViewModel.order.getValue());
            List<Section> list = mViewModel.sectionList.getValue();
            Collections.reverse(list);

            List<Section> newList = new ArrayList<>();
            newList.addAll(list);
            mViewModel.sectionList.setValue(newList);
            for (int i = 0; i < mViewModel.sectionList.getValue().size(); i++) {
                if (mViewModel.sectionList.getValue().get(i).id == mViewModel.currentSection.getValue().id) {
                    Message msg = new Message();
                    msg.what = 0;
                    msg.obj = i;
                    handler.sendMessageDelayed(msg, 500);
                }
            }
        }
    }

    private Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            DebugLog.e("<<<<<<<<<<<<<<<<<< " + (Integer) msg.obj);
            mViewModel.currentSectionPosition.setValue((Integer) msg.obj);
        }
    };

    public class EventHandler implements DefaultInterface.OnSeekBarChangeListener {

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            mPlayerManager.updateSeek(seekBar, progress, fromUser);
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
            mPlayerManager.startSeek(seekBar);
        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            mPlayerManager.seekTo(seekBar.getProgress());
        }
    }

    Handler interval_handler = new Handler();
    Runnable interval_Thread = new Runnable() {
        public void run() {
            nav().navigateUp();
        }
    };
}