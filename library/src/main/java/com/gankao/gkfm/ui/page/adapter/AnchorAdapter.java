package com.gankao.gkfm.ui.page.adapter;

import android.content.Context;

import androidx.core.content.ContextCompat;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import com.dragonpower.architecture.binding_recyclerview.adapter.SimpleDataBindingAdapter;
import com.gankao.gkfm.R;
import com.gankao.gkfm.data.bean.Anchor;
import com.gankao.gkfm.databinding.AdapterAnchorBinding;

public class AnchorAdapter extends SimpleDataBindingAdapter<Anchor.Filter.Children, AdapterAnchorBinding> {
    public MutableLiveData<Integer> currentPosition;

    public AnchorAdapter(Context context, MutableLiveData<Integer> currentPosition) {
        super(context, R.layout.adapter_anchor, DiffUtils.getInstance().getAnchorItemCallback());
        this.currentPosition = currentPosition;
    }

    @Override
    protected void onBindItem(AdapterAnchorBinding binding, Anchor.Filter.Children item, RecyclerView.ViewHolder holder) {
        binding.setVm(item);
        boolean pressed = currentPosition.getValue() == holder.getAbsoluteAdapterPosition();
        binding.tvOption.setBackgroundResource(pressed
                ? R.drawable.fm_anchor_selected : R.drawable.fm_anchor_noramal);
        binding.tvOption.setTextColor(
                ContextCompat.getColor(binding.getRoot().getContext(), pressed ? R.color.white : R.color.maincolor));
    }
}
