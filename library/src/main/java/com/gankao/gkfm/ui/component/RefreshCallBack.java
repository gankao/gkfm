package com.gankao.gkfm.ui.component;

public interface RefreshCallBack {
    void onLeftRefreshing();
    void onRightRefreshing();
}
