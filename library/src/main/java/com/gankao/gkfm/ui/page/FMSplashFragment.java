package com.gankao.gkfm.ui.page;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.dragonpower.architecture.http.EasyConfig;
import com.dragonpower.architecture.ui.page.BaseFragment;
import com.dragonpower.architecture.ui.page.DataBindingConfig;
import com.gankao.gkfm.BR;
import com.gankao.gkfm.R;
import com.gankao.gkfm.data.bean.Anchor;
import com.gankao.gkfm.data.bean.UserInfo;
import com.gankao.gkfm.data.config.Configs;
import com.gankao.gkfm.ui.page.adapter.AnchorAdapter;
import com.gankao.gkfm.ui.page.adapter.SectionAdapter;
import com.gankao.gkfm.ui.state.FMMainViewModel;
import com.gankao.gkfm.ui.state.SplashViewModel;

public class FMSplashFragment extends BaseFragment {


    public FMSplashFragment() {
    }

    private SplashViewModel mViewModel;
    private FMMainViewModel mDictationViewModel;
    private AnchorAdapter mAnchorAdapter;
    private SectionAdapter mSectionAdapter;

    @Override
    protected void initViewModel() {
        mViewModel = getFragmentScopeViewModel(SplashViewModel.class);
        mDictationViewModel = getActivityScopeViewModel(FMMainViewModel.class);
        mAnchorAdapter = new AnchorAdapter(getContext(), mViewModel.currentAnchorPosition);
        mAnchorAdapter.setOnItemClickListener((viewId, item, position) -> {
            mViewModel.currentAnchorPosition.setValue(position);
            mViewModel.currentAnchor.setValue(item);
            getSections();
        });
        mSectionAdapter = new SectionAdapter(getContext(), mViewModel.currentSectionPosition);
        mSectionAdapter.setOnItemClickListener((viewId, item, position) -> {
            if (mViewModel.sectionList.getValue().get(position).isShow) {
                Bundle bundle = new Bundle();
                bundle.putSerializable(Configs.PLAY_BUNDLE, item);
                nav().navigate(R.id.action_splashFragment_to_playerFragment, bundle);
            }
        });
    }

    @Override
    protected DataBindingConfig getDataBindingConfig() {
        return new DataBindingConfig(R.layout.fragment_fm_splash, BR.vm, mViewModel)
                .addBindingParam(BR.click, new ClickProxy())
                .addBindingParam(BR.adapter_anchor, mAnchorAdapter)
                .addBindingParam(BR.adapter_section, mSectionAdapter);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mViewModel.setLifecycleOwner(getViewLifecycleOwner());
        mViewModel.request.getUserInfoLiveData().observe(getViewLifecycleOwner(), dataResult -> {
            if (!dataResult.getResponseStatus().isSuccess()) return;
            UserInfo userInfo = dataResult.getResult();
            String token = userInfo.sdktoken;
            String[] arr = token.split(".");
            int firstSplit = token.indexOf(".");
            String userid = token.substring(0, firstSplit);
//            String token = data.substring(firstSplit + 1);
            EasyConfig.getInstance().addHeader("token", token)
                    .addHeader("Authorization", token)
                    .addHeader("user-id", userid);
            mViewModel.userInfo.setValue(userInfo);
            getAnchors();
        });
        mViewModel.request.getAnchorLiveData().observe(getViewLifecycleOwner(), dataResult -> {
            if (dataResult.getResponseStatus().isSuccess()) {
                mViewModel.anchorList.setValue(dataResult.getResult());
                mViewModel.currentAnchor.setValue(dataResult.getResult().get(0));
                getSections();
            }
        });
        mViewModel.request.getSectionLiveData().observe(getViewLifecycleOwner(), sections -> {
            mViewModel.sectionList.setValue(sections.data);
            mViewModel.refreshLayoutLoadMoreComplete.setValue(true);
            mViewModel.refreshLayoutRefreshComplete.setValue(true);
            mViewModel.notifySectionDataSetChanged.setValue(true);
        });
        mViewModel.request.getLastPageLoading().observe(getViewLifecycleOwner(), lastPage -> {
            if (lastPage) {
                mViewModel.refreshLayoutLoadMoreComplete.setValue(true);
            }
        });
        mViewModel.currentAnchorPosition.observe(getViewLifecycleOwner(), position -> {
            mViewModel.notifyAnchorDataSetChanged.setValue(true);
        });
    }

    public void doLogin() {
        if (mViewModel.userInfo.getValue() == null) {
            mViewModel.request.requestLogin(getViewLifecycleOwner());
        } else {
            getAnchors();
        }
    }

    public void getAnchors() {
        if (mViewModel.anchorList.getValue() == null || mViewModel.anchorList.getValue().size() == 0) {
            mViewModel.request.getAnchors(getViewLifecycleOwner());
        }
    }

    public void getSections() {
        if (mViewModel.currentAnchor.getValue() != null) {
            mViewModel.request.getSections(getViewLifecycleOwner(), mViewModel.currentAnchor.getValue());
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        doLogin();
    }

    @Override
    public void onPause() {
        super.onPause();
    }


    public class ClickProxy {

        public void back() {
            mDictationViewModel.requestToCloseActivityIfAllowed(true);
        }
    }
}