package com.gankao.gkfm.ui.page.adapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;

import com.gankao.gkfm.data.bean.Anchor;
import com.gankao.gkfm.data.bean.PlayerSpeed;
import com.gankao.gkfm.data.bean.PlayerTimeOff;
import com.gankao.gkfm.data.bean.Section;

public class DiffUtils {

    private DiffUtil.ItemCallback<Anchor.Filter.Children> mAnchorItemCallback;
    private DiffUtil.ItemCallback<Section> mSectionItemCallback;
    private DiffUtil.ItemCallback<PlayerTimeOff> mPlayerTimingOffItemCallback;
    private DiffUtil.ItemCallback<PlayerSpeed> mPlayerSpeedItemCallback;

    private DiffUtils() {
    }

    private static final DiffUtils S_DIFF_UTILS = new DiffUtils();

    public static DiffUtils getInstance() {
        return S_DIFF_UTILS;
    }

    public DiffUtil.ItemCallback<Anchor.Filter.Children> getAnchorItemCallback() {
        if (mAnchorItemCallback == null) {
            mAnchorItemCallback = new DiffUtil.ItemCallback<Anchor.Filter.Children>() {
                @Override
                public boolean areItemsTheSame(@NonNull Anchor.Filter.Children oldItem, @NonNull Anchor.Filter.Children newItem) {
                    return oldItem.equals(newItem);
                }

                @Override
                public boolean areContentsTheSame(@NonNull Anchor.Filter.Children oldItem, @NonNull Anchor.Filter.Children newItem) {
                    return oldItem.id.equals(newItem.id);
                }
            };
        }
        return mAnchorItemCallback;
    }

    public DiffUtil.ItemCallback<Section> getSectionItemCallback() {
        if (mSectionItemCallback == null) {
            mSectionItemCallback = new DiffUtil.ItemCallback<Section>() {
                @Override
                public boolean areItemsTheSame(@NonNull Section oldItem, @NonNull Section newItem) {
                    return oldItem.equals(newItem);
                }

                @Override
                public boolean areContentsTheSame(@NonNull Section oldItem, @NonNull Section newItem) {
                    return oldItem.id == (newItem.id);
                }
            };
        }
        return mSectionItemCallback;
    }

    public DiffUtil.ItemCallback<PlayerTimeOff> getPlayerTimingOffItemCallback() {
        if (mPlayerTimingOffItemCallback == null) {
            mPlayerTimingOffItemCallback = new DiffUtil.ItemCallback<PlayerTimeOff>() {
                @Override
                public boolean areItemsTheSame(@NonNull PlayerTimeOff oldItem, @NonNull PlayerTimeOff newItem) {
                    return oldItem.equals(newItem);
                }

                @Override
                public boolean areContentsTheSame(@NonNull PlayerTimeOff oldItem, @NonNull PlayerTimeOff newItem) {
                    return oldItem.id == (newItem.id);
                }
            };
        }
        return mPlayerTimingOffItemCallback;
    }

    public DiffUtil.ItemCallback<PlayerSpeed> getPlayerSpeedItemCallback() {
        if (mPlayerSpeedItemCallback == null) {
            mPlayerSpeedItemCallback = new DiffUtil.ItemCallback<PlayerSpeed>() {
                @Override
                public boolean areItemsTheSame(@NonNull PlayerSpeed oldItem, @NonNull PlayerSpeed newItem) {
                    return oldItem.equals(newItem);
                }

                @Override
                public boolean areContentsTheSame(@NonNull PlayerSpeed oldItem, @NonNull PlayerSpeed newItem) {
                    return oldItem.id == (newItem.id);
                }
            };
        }
        return mPlayerSpeedItemCallback;
    }

}
