package com.gankao.gkfm.ui.state;


import androidx.databinding.ObservableBoolean;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.gankao.gkfm.data.bean.Anchor;
import com.gankao.gkfm.data.bean.Section;
import com.gankao.gkfm.data.bean.UserInfo;
import com.gankao.gkfm.domain.request.SplashRequest;
import com.gankao.gkfm.ui.component.RefreshCallBack;
import com.gankao.gkfm.utils.DebugLog;

import java.util.List;

public class SplashViewModel extends ViewModel {
    private LifecycleOwner lifecycleOwner;

    public void setLifecycleOwner(LifecycleOwner lifecycleOwner) {
        this.lifecycleOwner = lifecycleOwner;
    }

    public final MutableLiveData<UserInfo> userInfo = new MutableLiveData<>();

    public final MutableLiveData<List<Anchor.Filter.Children>> anchorList = new MutableLiveData<>();
    public final MutableLiveData<Boolean> notifyAnchorDataSetChanged = new MutableLiveData();
    public final MutableLiveData<List<Section>> sectionList = new MutableLiveData<>();
    public final MutableLiveData<Boolean> notifySectionDataSetChanged = new MutableLiveData();

    public final MutableLiveData<Integer> currentAnchorPosition = new MutableLiveData<>();
    public final MutableLiveData<Integer> currentSectionPosition = new MutableLiveData<>();
    public final MutableLiveData<Anchor.Filter.Children> currentAnchor = new MutableLiveData<>();

    public final MutableLiveData<Boolean> refreshLayoutLoadMoreComplete = new MutableLiveData();
    public final MutableLiveData<Boolean> refreshLayoutRefreshComplete = new MutableLiveData();
    public final MutableLiveData<RefreshCallBack> refreshListener = new MutableLiveData();

    RefreshCallBack refreshCallBack = new RefreshCallBack() {
        @Override
        public void onLeftRefreshing() {
            DebugLog.e("onLeftRefreshing");
            request.getSections(lifecycleOwner, currentAnchor.getValue());
        }

        @Override
        public void onRightRefreshing() {
            DebugLog.e("onRightRefreshing");
            request.getSectionsNextPage(lifecycleOwner, currentAnchor.getValue());
        }
    };

    public final SplashRequest request = new SplashRequest();

    {
        refreshListener.setValue(refreshCallBack);
        currentAnchorPosition.setValue(0);
        currentSectionPosition.setValue(0);
    }
}
