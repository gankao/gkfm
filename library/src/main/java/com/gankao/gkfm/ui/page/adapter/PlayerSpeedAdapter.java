package com.gankao.gkfm.ui.page.adapter;

import android.content.Context;
import android.view.View;

import androidx.core.content.ContextCompat;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import com.dragonpower.architecture.binding_recyclerview.adapter.SimpleDataBindingAdapter;
import com.gankao.gkfm.R;
import com.gankao.gkfm.data.bean.PlayerSpeed;
import com.gankao.gkfm.databinding.AdapterPlayerSpeedBinding;

public class PlayerSpeedAdapter extends SimpleDataBindingAdapter<PlayerSpeed, AdapterPlayerSpeedBinding> {
    public MutableLiveData<Integer> currentPosition;

    public PlayerSpeedAdapter(Context context, MutableLiveData<Integer> currentPosition) {
        super(context, R.layout.adapter_player_speed, DiffUtils.getInstance().getPlayerSpeedItemCallback());
        this.currentPosition = currentPosition;
    }

    @Override
    protected void onBindItem(AdapterPlayerSpeedBinding binding, PlayerSpeed item, RecyclerView.ViewHolder holder) {
        binding.setVm(item);
        binding.driver.setVisibility(holder.getAbsoluteAdapterPosition() == getItemCount() - 1 ? View.GONE : View.VISIBLE);
        binding.tvTitle.setTextColor(ContextCompat.getColor(binding.getRoot().getContext(), holder.getAbsoluteAdapterPosition() == currentPosition.getValue()
                ? R.color.light_blue : R.color.white));
    }
}
