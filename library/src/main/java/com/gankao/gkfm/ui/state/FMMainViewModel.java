package com.gankao.gkfm.ui.state;

import androidx.lifecycle.ViewModel;

import com.dragonpower.architecture.ui.callback.ProtectedUnPeekLiveData;
import com.dragonpower.architecture.ui.callback.UnPeekLiveData;

public class FMMainViewModel extends ViewModel {
    private final UnPeekLiveData<Boolean> toCloseActivityIfAllowed = new UnPeekLiveData<>();

    public ProtectedUnPeekLiveData<Boolean> isToCloseActivityIfAllowed() {
        return toCloseActivityIfAllowed;
    }

    public void requestToCloseActivityIfAllowed(boolean allow) {
        toCloseActivityIfAllowed.setValue(allow);
    }
}
