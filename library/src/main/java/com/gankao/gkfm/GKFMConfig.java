package com.gankao.gkfm;

import android.app.Activity;
import android.content.Context;
import android.os.Build;

import com.dragonpower.architecture.http.EasyConfig;
import com.dragonpower.architecture.http.config.IRequestApi;
import com.dragonpower.architecture.http.config.IRequestInterceptor;
import com.dragonpower.architecture.http.config.IRequestServer;
import com.dragonpower.architecture.http.model.HttpHeaders;
import com.dragonpower.architecture.http.model.HttpParams;
import com.dragonpower.architecture.utils.DeviceUtils;
import com.dragonpower.architecture.utils.Utils;
import com.gankao.gkfm.http.model.RequestHandler;
import com.gankao.gkfm.http.server.ReleaseServer;

import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;


public class GKFMConfig {

    private static Activity sActivity;
    private static String mpartnerId;
    private static String mmarkId;
    private static String msecret;

    private static Context mApplication;
    private static List<Activity> activities;

    public static String getPartnerId() {
        return mpartnerId;
    }

    public static String getMarkID() {
        return mmarkId;
    }

    public static String getSecret() {
        return msecret;
    }

    public static void addActivity(Activity activity){
        activities.add(activity);
    }
    public static void removeActivity(Activity activity){
        if (activities!=null && activities.size()> 0){

            activities.remove(activity);
        }
    }

    public static void finish(){
        if (activities==null || activities.size() < 1){
            return;
        }
        for (Activity activity:activities){
            activity.finish();
        }
        activities.clear();
    }
    public static void init(Context context, String partnerId, String mardId, String secret) {
        mApplication = context;
        Utils.init(context);
        mpartnerId = partnerId;
        mmarkId = mardId;
        msecret = secret;
        activities = new ArrayList<>();
        IRequestServer server = new ReleaseServer();
//        if (BuildConfig.DEBUG) {
//            server = new TestServer();
//        } else {
//            server = new ReleaseServer();
//        }

        OkHttpClient okHttpClient = new OkHttpClient.Builder().build();

        EasyConfig.with(okHttpClient)
                .setLogEnabled(true)
                .setServer(server)
                .setHandler(new RequestHandler(Utils.getApp()))
                .setInterceptor(new IRequestInterceptor() {
                    @Override
                    public void interceptArguments(IRequestApi api, HttpParams params, HttpHeaders headers) {
                        headers.put("timestamp", String.valueOf(System.currentTimeMillis()));
                    }
                })
                .setRetryCount(1)
                .setRetryTime(2000)
                .addHeader("Accept-Encoding", "gzip")
                .addHeader("dev-id", DeviceUtils.getDeviceId(mApplication))
                .addHeader("dev-name", Build.MODEL)
                .addHeader("system-verison", "Android " + Build.VERSION.RELEASE + "(" + Build.VERSION.SDK_INT + ")")
                .addHeader("app-version", DeviceUtils.getVersionName(mApplication))
                .addHeader("appid", "gankaoDictation")
                .addHeader("app-type", "1")
                .addHeader("phone-model", Build.BRAND + " " + Build.MODEL + "")
                .addHeader("app-channel", "gankao")
                .addHeader("ts", System.currentTimeMillis() / 1000 + "")
                .addHeader("macaddr", DeviceUtils.getMacAddress(mApplication))
                .into();
    }


    public static Activity getActivity() {
        return sActivity;
    }

    public static void setsActivity(Activity sActivity) {
        GKFMConfig.sActivity = sActivity;
    }

    public static Context getContext() {
        return mApplication;
    }


}
