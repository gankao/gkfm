package com.gankao.gkfm;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.dragonpower.architecture.ui.page.BaseActivity;
import com.dragonpower.architecture.ui.page.DataBindingConfig;
import com.gankao.gkfm.ui.state.FMMainViewModel;

import me.jessyan.autosize.internal.CustomAdapt;

public class GKFMMainActivity extends BaseActivity implements CustomAdapt {

    private FMMainViewModel mState;

    @Override
    protected void initViewModel() {
        mState = getActivityScopeViewModel(FMMainViewModel.class);
    }

    @Override
    protected DataBindingConfig getDataBindingConfig() {
        return new DataBindingConfig(R.layout.fm_activity_main, BR.mainvm, mState);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GKFMConfig.addActivity(this);
        setFullScreen(this);
        mState.isToCloseActivityIfAllowed().observeInActivity(this, aBoolean -> {
            NavController nav = Navigation.findNavController(this, R.id.main_fragment_host);
            if (nav.getCurrentDestination() != null && nav.getCurrentDestination().getId() != R.id.splashFragment) {
                nav.navigateUp();
            } else {
                super.onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        mState.requestToCloseActivityIfAllowed(true);
    }

    @Override
    public boolean isBaseOnWidth() {
        return true;
    }

    @Override
    public float getSizeInDp() {
        return 960;
    }


    public void setFullScreen(Activity activity) {
        WindowManager.LayoutParams params = activity.getWindow()
                .getAttributes();
        params.flags |= WindowManager.LayoutParams.FLAG_FULLSCREEN;
        activity.getWindow().setAttributes(params);
        activity.getWindow().addFlags(
                WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }


    @Override
    protected void onStop() {
        super.onStop();
        GKFMConfig.removeActivity(this);
    }
}