package com.gankao.gkfm.data.bean;

import java.util.List;

public class Anchor {
    public List<Filter> filters;

    public class Filter {
        public String typeName;
        public List<Children> children;

        public class Children {
            public String id;
            public String name;
        }
    }
}
