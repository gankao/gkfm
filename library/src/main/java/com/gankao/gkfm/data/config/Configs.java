package com.gankao.gkfm.data.config;

import android.os.Environment;

import com.dragonpower.architecture.utils.Utils;

public class Configs {

    public static final String COVER_PATH = Utils.getApp().getExternalFilesDir(Environment.DIRECTORY_PICTURES).getAbsolutePath();

    public static final String PLAY_BUNDLE = "player_section";
    public static final String TOKEN = "token";

}
