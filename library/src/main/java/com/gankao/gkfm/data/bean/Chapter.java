package com.gankao.gkfm.data.bean;

public class Chapter {
    public int id;
    public String name;
    public String title_pic;
    public String views;
    public String teacher_name;
    public String title_pic_hd;
    public String praiseCount;
    public String commentSubjectKey;
}
