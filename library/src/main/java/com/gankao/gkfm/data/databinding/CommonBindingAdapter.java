package com.gankao.gkfm.data.databinding;

import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SeekBar;

import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.GenericTransitionOptions;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.dragonpower.architecture.binding_recyclerview.layout_manager.WrapContentGridLayoutManager;
import com.dragonpower.architecture.binding_recyclerview.layout_manager.WrapContentLinearLayoutManager;
import com.dragonpower.architecture.utils.DisplayUtils;
import com.gankao.gkfm.R;
import com.gankao.gkfm.ui.component.HorizontalRefreshLayout;
import com.gankao.gkfm.ui.component.RefreshCallBack;
import com.gankao.gkfm.ui.component.refreshhead.LoadingRefreshHeader;
import com.gankao.gkfm.utils.DebugLog;

public class CommonBindingAdapter {

    @BindingAdapter(value = {"touchChange"}, requireAll = false)
    public static void touchChange(View view, int resId) {
        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        v.setAlpha(0.7f);
                        break;
                    case MotionEvent.ACTION_UP:
                    case MotionEvent.ACTION_CANCEL:
                        v.setAlpha(1.0f);
                        break;
                }
                return false;
            }
        });
    }

    @BindingAdapter(value = {"touchWhiteChange"}, requireAll = false)
    public static void touchWhiteChange(View view, int resId) {
        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        v.setBackgroundResource(R.drawable.fm_bg_touch_20);
                        break;
                    case MotionEvent.ACTION_UP:
                    case MotionEvent.ACTION_CANCEL:
                        v.setBackgroundResource(R.drawable.fm_bg_white_20);
                        break;
                }
                return false;
            }
        });
    }

    @BindingAdapter(value = {"imageUrl"}, requireAll = false)
    public static void imageUrl(ImageView view, String url) {
        Glide.with(view.getContext()).load(url).transition(GenericTransitionOptions.with(R.anim.h_imageview_enter))
                .into(view);
    }

    @BindingAdapter(value = {"imageRes"}, requireAll = false)
    public static void imageRes(ImageView view, int resId) {
        Glide.with(view.getContext()).asDrawable().load(resId).into(view);
    }

    @BindingAdapter(value = {"imageRes"}, requireAll = false)
    public static void imageRes(ImageView view, String localRes) {
        int res = view.getContext().getResources().getIdentifier(localRes.substring(9, localRes.length()), "mipmap", view.getContext().getPackageName());
        Glide.with(view.getContext()).load(res).into(view);
    }

    @BindingAdapter(value = {"rectangleImageUrl"}, requireAll = false)
    public static void halfRectangleImageUrl(ImageView view, String url) {
        int radius = DisplayUtils.dp2px(20);
        Glide.with(view.getContext()).load(url)
                .apply(RequestOptions.bitmapTransform(new RoundedCorners(radius))).dontAnimate().into(view);
    }

    @BindingAdapter(value = {"initRefreshLayout"}, requireAll = false)
    public static void initRefreshLayout(HorizontalRefreshLayout refreshLayout, RefreshCallBack listener) {
        refreshLayout.setRefreshCallback(listener);
        refreshLayout.setRefreshHeader(new LoadingRefreshHeader(refreshLayout.getContext()), HorizontalRefreshLayout.LEFT);
        refreshLayout.setRefreshHeader(new LoadingRefreshHeader(refreshLayout.getContext()), HorizontalRefreshLayout.RIGHT);
    }

    @BindingAdapter(value = {"onLoadMoreComplete"}, requireAll = false)
    public static void onLoadMoreComplete(HorizontalRefreshLayout refreshLayout, boolean complete) {
        if (complete) {
            refreshLayout.onRefreshComplete();
        }
    }

    @BindingAdapter(value = {"onRefreshComplete"}, requireAll = false)
    public static void onRefreshComplete(HorizontalRefreshLayout refreshLayout, boolean complete) {
        if (complete) {
            refreshLayout.onRefreshComplete();
        }
    }

    @BindingAdapter(value = {"moveToPosition"}, requireAll = false)
    public static void moveToPosition(RecyclerView recyclerView, int position) {
        try {
            if (recyclerView.getLayoutManager() instanceof WrapContentGridLayoutManager) {
                WrapContentGridLayoutManager layoutManager = (WrapContentGridLayoutManager) recyclerView.getLayoutManager();
                int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();
                int lastVisibleItemPosition = layoutManager.findLastVisibleItemPosition();
                View firstVisibleItem = layoutManager.findViewByPosition(firstVisibleItemPosition);
                View lastVisibleItem = layoutManager.findViewByPosition(lastVisibleItemPosition);
                View view = layoutManager.findViewByPosition(position);
                int spanCount = ((WrapContentGridLayoutManager) recyclerView.getLayoutManager()).getSpanCount();
                if (firstVisibleItem != null) {
                    int firstTop = firstVisibleItem.getTop();
                    int lastBottom = lastVisibleItem.getBottom();
                    int height = recyclerView.getHeight();
                    ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) firstVisibleItem.getLayoutParams();
                    int margin = layoutParams.topMargin + layoutParams.bottomMargin;
                    int itemHeight = firstVisibleItem.getHeight();
                    int firstRow = firstVisibleItemPosition / spanCount;
                    int currentRow = position / spanCount;
                    int lastRow = lastVisibleItemPosition / spanCount;

                    if (view == null) {
                        if (currentRow < firstRow) {
                            int distance = (currentRow - firstRow) * (itemHeight + margin) + firstTop;
                            recyclerView.smoothScrollBy(0, distance);
                        } else if (currentRow > lastRow) {
                            int distance = (currentRow - lastRow) * (itemHeight + margin) + (lastBottom - itemHeight + margin);
                            recyclerView.smoothScrollBy(0, distance);
                        }
                    } else {
                        int targetItemTop = view.getTop();
                        int targetItemBottom = view.getBottom();
                        if (currentRow == firstRow) {
                            recyclerView.smoothScrollBy(0, targetItemTop - itemHeight / 2);
                        } else if (currentRow == lastRow) {
                            recyclerView.smoothScrollBy(0, targetItemBottom - height + itemHeight / 2);
                        }
                    }
                }
            } else if (recyclerView.getLayoutManager() instanceof WrapContentLinearLayoutManager) {
                WrapContentLinearLayoutManager layoutManager = (WrapContentLinearLayoutManager) recyclerView.getLayoutManager();
                if (layoutManager.getOrientation() == RecyclerView.HORIZONTAL) {
                    int distance = 0;
                    int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();
                    int lastVisibleItemPosition = layoutManager.findLastVisibleItemPosition();
                    View firstVisibleItem = layoutManager.findViewByPosition(firstVisibleItemPosition);
                    View lastVisibleItem = layoutManager.findViewByPosition(lastVisibleItemPosition);
                    View view = layoutManager.findViewByPosition(position);
                    if (firstVisibleItem == null) {

                    } else {
                        int left = firstVisibleItem.getLeft();
                        int right = lastVisibleItem.getRight();
                        int width = recyclerView.getWidth();
                        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) firstVisibleItem.getLayoutParams();
                        int itemWidth = lastVisibleItem.getWidth();
                        int margin = layoutParams.rightMargin + layoutParams.leftMargin;
                        distance = (position - firstVisibleItemPosition) * (itemWidth + margin) + left - width / 2
                                + (firstVisibleItemPosition == 0 ? (DisplayUtils.dp2px(24)) : 0)
                                + (itemWidth - DisplayUtils.dp2px(12)) / 2;
                        recyclerView.smoothScrollBy(distance, 0);
                        DebugLog.e("----- " + firstVisibleItemPosition + ", " + position + ", " + lastVisibleItemPosition);
//                        if (view == null) {
//                            if (position < firstVisibleItemPosition) {
//                                distance = (position - firstVisibleItemPosition) * (itemWidth + margin) + left - paddingLeft;
//
//                            } else if (position > lastVisibleItemPosition) {
//                                distance = (position - lastVisibleItemPosition) * (itemWidth + margin) + (right - width + margin);
//                                DebugLog.e(distance);
//                                recyclerView.smoothScrollBy(distance, 0);
//                            }
//                        } else {
//                            int targetItemLeft = view.getLeft();
//                            int targetItemRight = view.getRight();
//                            if (position == firstVisibleItemPosition) {
//                                recyclerView.smoothScrollBy(targetItemLeft - itemWidth / 2, 0);
//                            } else if (position == lastVisibleItemPosition) {
//                                recyclerView.smoothScrollBy(targetItemRight - width + itemWidth / 2, 0);
//                            }
//                        }
                    }
                } else if (layoutManager.getOrientation() == RecyclerView.VERTICAL) {
                    int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();
                    int lastVisibleItemPosition = layoutManager.findLastVisibleItemPosition();

                    View firstVisibleItem = layoutManager.findViewByPosition(firstVisibleItemPosition);
                    View lastVisibleItem = layoutManager.findViewByPosition(lastVisibleItemPosition);
                    View view = layoutManager.findViewByPosition(position);
                    if (firstVisibleItem == null) {

                    } else {
                        int left = firstVisibleItem.getTop();
                        int right = lastVisibleItem.getBottom();
                        int width = recyclerView.getHeight();
                        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) firstVisibleItem.getLayoutParams();
                        int itemWidth = firstVisibleItem.getHeight();
                        int margin = layoutParams.topMargin + layoutParams.bottomMargin;
                        if (view == null) {
                            if (position < firstVisibleItemPosition) {
                                int distance = (position - firstVisibleItemPosition) * (itemWidth + margin) + left;
                                recyclerView.smoothScrollBy(0, distance);
                            } else if (position > lastVisibleItemPosition) {
                                int distance = (position - lastVisibleItemPosition) * (itemWidth + margin) + (right - width + margin);
                                recyclerView.smoothScrollBy(0, distance);
                            }
                        } else {
                            int targetItemLeft = view.getTop();
                            int targetItemRight = view.getBottom();
                            if (position == firstVisibleItemPosition) {
                                recyclerView.smoothScrollBy(0, targetItemLeft - itemWidth / 2);
                            } else if (position == lastVisibleItemPosition) {
                                recyclerView.smoothScrollBy(0, targetItemRight - width + itemWidth / 2);
                            }
                        }
                    }
                } else {
                    layoutManager.scrollToPositionWithOffset(0, position);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @BindingAdapter(value = {"onSeekBarChangeListener"}, requireAll = false)
    public static void onSeekBarChangeListener(SeekBar seekBar, SeekBar.OnSeekBarChangeListener listener) {
        seekBar.setOnSeekBarChangeListener(listener);
    }
}
