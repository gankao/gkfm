package com.gankao.gkfm.data.repository;

import androidx.lifecycle.LifecycleOwner;

import com.dragonpower.architecture.data.response.DataResult;
import com.dragonpower.architecture.data.response.ResponseStatus;
import com.dragonpower.architecture.http.EasyHttp;
import com.dragonpower.architecture.http.listener.HttpCallback;
import com.dragonpower.architecture.http.listener.OnHttpListener;
import com.dragonpower.architecture.utils.Utils;
import com.gankao.gkfm.GKFMConfig;
import com.gankao.gkfm.R;
import com.gankao.gkfm.data.bean.Anchor;
import com.gankao.gkfm.data.bean.GrowCourse;
import com.gankao.gkfm.data.bean.PagingData;
import com.gankao.gkfm.data.bean.PlayerSpeed;
import com.gankao.gkfm.data.bean.PlayerTimeOff;
import com.gankao.gkfm.data.bean.Section;
import com.gankao.gkfm.data.bean.UserInfo;
import com.gankao.gkfm.http.api.AnchorListApi;
import com.gankao.gkfm.http.api.GrowCourseApi;
import com.gankao.gkfm.http.api.LoginApi;
import com.gankao.gkfm.http.api.SectionApi;
import com.gankao.gkfm.http.model.HttpData;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

public class DataRepository implements ILocalSource, IRemoteSource, OnHttpListener {

    private static final DataRepository S_REQUEST_MANAGER = new DataRepository();

    private DataRepository() {
    }

    public static DataRepository getInstance() {
        return S_REQUEST_MANAGER;
    }

    @Override
    public void onSucceed(Object result) {

    }

    @Override
    public void onFail(Exception e) {

    }

    public void requestLogin(LifecycleOwner lifecycleOwner, DataResult.Result<UserInfo> dataResult) {
        EasyHttp.get(lifecycleOwner)
                .api(new LoginApi().setLoginInfo(GKFMConfig.getPartnerId(), GKFMConfig.getMarkID(), GKFMConfig.getSecret()))
                .request(new HttpCallback<HttpData<UserInfo>>(this) {

                    @Override
                    public void onSucceed(HttpData<UserInfo> result) {
                        dataResult.onResult(new DataResult<>(result.getData(), new ResponseStatus()));
                    }

                    @Override
                    public void onFail(Exception e) {
                        super.onFail(e);
                        ResponseStatus responseStatus = new ResponseStatus("400", false, e.getMessage());
                        dataResult.onResult(new DataResult(null, responseStatus));
                    }
                });
    }

    public void requestAnchors(LifecycleOwner lifecycleOwner, DataResult.Result<List<Anchor.Filter.Children>> dataResult) {
        EasyHttp.get(lifecycleOwner)
                .api(new AnchorListApi())
                .request(new HttpCallback<HttpData<Anchor>>(this) {

                    @Override
                    public void onSucceed(HttpData<Anchor> result) {
                        dataResult.onResult(new DataResult<>(result.getData().filters.get(0).children, new ResponseStatus()));
                    }

                    @Override
                    public void onFail(Exception e) {
                        super.onFail(e);
                        ResponseStatus responseStatus = new ResponseStatus("400", false, e.getMessage());
                        dataResult.onResult(new DataResult(null, responseStatus));
                    }
                });
    }

    public void requestSections(LifecycleOwner lifecycleOwner, Anchor.Filter.Children anchor, int page, DataResult.Result<PagingData<Section>> dataResult) {
        EasyHttp.get(lifecycleOwner)
                .api(new SectionApi().setParams(anchor.id, page))
                .request(new HttpCallback<HttpData<PagingData<Section>>>(this) {

                    @Override
                    public void onSucceed(HttpData<PagingData<Section>> result) {
                        dataResult.onResult(new DataResult<>(result.getData(), new ResponseStatus()));
                    }

                    @Override
                    public void onFail(Exception e) {
                        super.onFail(e);
                        ResponseStatus responseStatus = new ResponseStatus("400", false, e.getMessage());
                        dataResult.onResult(new DataResult(null, responseStatus));
                    }
                });
    }

    public void requestTimeOffList(LifecycleOwner lifecycleOwner, DataResult.Result<List<PlayerTimeOff>> dataResult) {
        Gson gson = new Gson();
        Type type = new TypeToken<List<PlayerTimeOff>>() {
        }.getType();
        List<PlayerTimeOff> list = gson.fromJson(Utils.getApp().getString(R.string.timingoff_list_json), type);
        ResponseStatus responseStatus = new ResponseStatus("200", true);
        dataResult.onResult(new DataResult<>(list, responseStatus));
    }

    public void requestSpeedList(LifecycleOwner lifecycleOwner, DataResult.Result<List<PlayerSpeed>> dataResult) {
        Gson gson = new Gson();
        Type type = new TypeToken<List<PlayerSpeed>>() {
        }.getType();
        List<PlayerSpeed> list = gson.fromJson(Utils.getApp().getString(R.string.speed_list_json), type);
        ResponseStatus responseStatus = new ResponseStatus("200", true);
        dataResult.onResult(new DataResult<>(list, responseStatus));
    }

    public void requestPlayerSections(LifecycleOwner lifecycleOwner, Section section, DataResult.Result<GrowCourse> dataResult) {
        EasyHttp.get(lifecycleOwner)
                .api(new GrowCourseApi().setParams(section.id))
                .request(new HttpCallback<HttpData<GrowCourse>>(this) {

                    @Override
                    public void onSucceed(HttpData<GrowCourse> result) {
                        dataResult.onResult(new DataResult<>(result.getData(), new ResponseStatus()));
                    }

                    @Override
                    public void onFail(Exception e) {
                        super.onFail(e);
                        ResponseStatus responseStatus = new ResponseStatus("400", false, e.getMessage());
                        dataResult.onResult(new DataResult(null, responseStatus));
                    }
                });
    }

}
