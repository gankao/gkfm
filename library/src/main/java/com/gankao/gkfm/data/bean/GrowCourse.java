package com.gankao.gkfm.data.bean;

import java.util.List;

public class GrowCourse {
    public String type;
    public Section course;
    public List<Section> fmSections;
}
