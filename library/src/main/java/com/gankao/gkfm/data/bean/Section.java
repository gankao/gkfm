package com.gankao.gkfm.data.bean;

import java.io.Serializable;

public class Section implements Serializable {
    public int id;
    public String name;
    public String title_pic;
    public String created_at;
    public String title_pic_hd;
    public String descriptionUrl;
    public String src;
    public String commentSubjectKey;
    public boolean isShow = true;
}
