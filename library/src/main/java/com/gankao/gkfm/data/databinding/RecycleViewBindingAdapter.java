package com.gankao.gkfm.data.databinding;

import android.graphics.Rect;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.BindingAdapter;
import androidx.databinding.InverseBindingAdapter;
import androidx.databinding.InverseBindingListener;
import androidx.databinding.InverseBindingMethod;
import androidx.databinding.InverseBindingMethods;
import androidx.recyclerview.widget.RecyclerView;

import com.dragonpower.architecture.utils.DisplayUtils;
import com.gankao.gkfm.utils.DebugLog;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.constant.RefreshState;
import com.scwang.smart.refresh.layout.listener.OnLoadMoreListener;
import com.scwang.smart.refresh.layout.listener.OnRefreshListener;

import org.jetbrains.annotations.NotNull;

@InverseBindingMethods({
        @InverseBindingMethod(type = SmartRefreshLayout.class, attribute = "refreshing", method = "isRefreshing"),
        @InverseBindingMethod(type = SmartRefreshLayout.class, attribute = "loadmore", method = "isLoading"),
})
public class RecycleViewBindingAdapter {

    @BindingAdapter(value = {"itemDecoration"}, requireAll = false)
    public static void itemDecoration(RecyclerView recyclerView, int offset) {
        recyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                super.getItemOffsets(outRect, view, parent, state);
                outRect.right = DisplayUtils.dp2px(offset);
                outRect.bottom = DisplayUtils.dp2px(offset);
            }
        });
    }

    @BindingAdapter(value = {"refreshing"}, requireAll = false)
    public static void refreshing(SmartRefreshLayout smartRefreshLayout, boolean refresh) {
        DebugLog.e("refreshing >>>>> " + smartRefreshLayout.getState() + ", " + refresh);
        if ((smartRefreshLayout.getState() == RefreshState.Refreshing) != refresh) {
            smartRefreshLayout.finishRefresh();
        }
    }

    @BindingAdapter(value = {"loadmore"}, requireAll = false)
    public static void loadmore(SmartRefreshLayout smartRefreshLayout, boolean loadmore) {
        DebugLog.e("loadmore >>>>> " + (smartRefreshLayout.getState()) + ", " + loadmore);
        if ((smartRefreshLayout.getState() == RefreshState.Loading) != loadmore) {
            smartRefreshLayout.finishLoadMore();
        }
    }

    @InverseBindingAdapter(attribute = "refreshing")
    public static boolean isRefreshing(SmartRefreshLayout smartRefreshLayout) {
        DebugLog.e("isRefreshing >>>>> " + (smartRefreshLayout.getState() == RefreshState.Refreshing));
        return (smartRefreshLayout.getState() == RefreshState.Refreshing);
    }

    @InverseBindingAdapter(attribute = "loadmore")
    public static boolean isLoadMore(SmartRefreshLayout smartRefreshLayout) {
        DebugLog.e("isLoadMore >>>>> " + (smartRefreshLayout.getState() == RefreshState.Loading));
        return (smartRefreshLayout.getState() == RefreshState.Loading);
    }

    @BindingAdapter(value = {"onRefreshListener", "refreshingAttrChanged"},
            requireAll = false)
    public static void setOnRefreshListener(SmartRefreshLayout smartRefreshLayout, final OnRefreshListener listener,
                                            final InverseBindingListener attrChange) {
        DebugLog.e("onRefreshListener >>>>> " + (attrChange == null));
        if (attrChange == null) {
            smartRefreshLayout.setOnRefreshListener(listener);
        } else {
            smartRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
                @Override
                public void onRefresh(@NonNull @NotNull RefreshLayout refreshLayout) {
                    if (listener != null) {
                        listener.onRefresh(refreshLayout);
                    }
                    attrChange.onChange();
                }
            });
        }
    }

    @BindingAdapter(value = {"onLoadMoreListener", "loadmoreAttrChanged"},
            requireAll = false)
    public static void setOnLoadMoreListener(SmartRefreshLayout smartRefreshLayout, final OnLoadMoreListener listener,
                                             final InverseBindingListener attrChange) {
        DebugLog.e("onLoadMoreListener >>>>> " + (attrChange == null));
        if (attrChange == null) {
            smartRefreshLayout.setOnLoadMoreListener(listener);
        } else {
            smartRefreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
                @Override
                public void onLoadMore(@NonNull @NotNull RefreshLayout refreshLayout) {
                    if (listener != null) {
                        listener.onLoadMore(refreshLayout);
                    }
                    attrChange.onChange();
                }
            });
        }
    }
}
