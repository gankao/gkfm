package com.gankao.gkfm.domain.request;

import android.util.Log;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.dragonpower.architecture.data.response.DataResult;
import com.dragonpower.architecture.domain.request.BaseRequest;
import com.gankao.gkfm.R;
import com.gankao.gkfm.data.bean.Anchor;
import com.gankao.gkfm.data.bean.PagingData;
import com.gankao.gkfm.data.bean.Section;
import com.gankao.gkfm.data.bean.UserInfo;
import com.gankao.gkfm.data.repository.DataRepository;
import com.gankao.gkfm.utils.DebugLog;

import java.util.List;


public class SplashRequest extends BaseRequest {

    private MutableLiveData<DataResult<UserInfo>> userInfoLiveData;
    private MutableLiveData<DataResult<List<Anchor.Filter.Children>>> anchorLiveData;
    private MutableLiveData<PagingData<Section>> sectionLiveData;
    private MutableLiveData<Boolean> lastPageLoading;
    private int page = 1;

    public LiveData<DataResult<UserInfo>> getUserInfoLiveData() {
        if (userInfoLiveData == null) {
            userInfoLiveData = new MutableLiveData<>();
        }
        return userInfoLiveData;
    }

    public LiveData<DataResult<List<Anchor.Filter.Children>>> getAnchorLiveData() {
        if (anchorLiveData == null) {
            anchorLiveData = new MutableLiveData<>();
        }
        return anchorLiveData;
    }

    public LiveData<PagingData<Section>> getSectionLiveData() {
        if (sectionLiveData == null) {
            sectionLiveData = new MutableLiveData<>();
        }
        return sectionLiveData;
    }

    public LiveData<Boolean> getLastPageLoading() {
        if (lastPageLoading == null) {
            lastPageLoading = new MutableLiveData<>();
        }
        return lastPageLoading;
    }

    public void requestLogin(LifecycleOwner lifecycleOwner) {
        DataRepository.getInstance().requestLogin(lifecycleOwner, userInfoLiveData::postValue);
    }

    public void getAnchors(LifecycleOwner lifecycleOwner) {
        DataRepository.getInstance().requestAnchors(lifecycleOwner, anchorLiveData::postValue);
    }

    public void getSections(LifecycleOwner lifecycleOwner, Anchor.Filter.Children anchor) {
        page = 1;
        DataRepository.getInstance().requestSections(lifecycleOwner, anchor, page, new DataResult.Result<PagingData<Section>>() {
            @Override
            public void onResult(DataResult<PagingData<Section>> dataResult) {
                if (dataResult.getResponseStatus().isSuccess()) {
                    if (dataResult.getResult().total < 3){

                       for (int i = 0;i<3-dataResult.getResult().total;i++){
                           Section section = new Section();
                           section.title_pic = "";
                           section.title_pic_hd = "";
                           section.isShow = false;
                           dataResult.getResult().data.add(section);
                       }
                    }

                    sectionLiveData.setValue(dataResult.getResult());
                }
            }
        });
    }

    public void getSectionsNextPage(LifecycleOwner lifecycleOwner, Anchor.Filter.Children anchor) {
        if (sectionLiveData.getValue() == null) return;
        PagingData pagingData = sectionLiveData.getValue();
        if (pagingData != null) {
            if (page < pagingData.last_page) {
                int targetPage = page + 1;
                DataRepository.getInstance().requestSections(lifecycleOwner, anchor, targetPage, new DataResult.Result<PagingData<Section>>() {
                    @Override
                    public void onResult(DataResult<PagingData<Section>> dataResult) {
                        if (dataResult.getResponseStatus().isSuccess()) {
                            page = targetPage;
                            PagingData current = sectionLiveData.getValue();
                            current.data.addAll(dataResult.getResult().data);
                            sectionLiveData.setValue(current);
                        }
                    }
                });
            } else {
                if (pagingData.total > 0) {
                    lastPageLoading.setValue(true);
                }
            }
        }

    }
}
