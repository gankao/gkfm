package com.gankao.gkfm.domain.request;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.dragonpower.architecture.data.response.DataResult;
import com.dragonpower.architecture.domain.request.BaseRequest;
import com.gankao.gkfm.data.bean.GrowCourse;
import com.gankao.gkfm.data.bean.PlayerSpeed;
import com.gankao.gkfm.data.bean.PlayerTimeOff;
import com.gankao.gkfm.data.bean.Section;
import com.gankao.gkfm.data.repository.DataRepository;

import java.util.List;


public class PlayerRequest extends BaseRequest {

    private MutableLiveData<DataResult<List<PlayerTimeOff>>> timeOffLiveData;
    private MutableLiveData<DataResult<List<PlayerSpeed>>> speedLiveData;
    private MutableLiveData<DataResult<GrowCourse>> growCourseLiveData;

    public LiveData<DataResult<List<PlayerTimeOff>>> getTimingOffLiveData() {
        if (timeOffLiveData == null) {
            timeOffLiveData = new MutableLiveData<>();
        }
        return timeOffLiveData;
    }

    public LiveData<DataResult<List<PlayerSpeed>>> getSpeedLiveData() {
        if (speedLiveData == null) {
            speedLiveData = new MutableLiveData<>();
        }
        return speedLiveData;
    }

    public LiveData<DataResult<GrowCourse>> getGrowCourseLiveData() {
        if (growCourseLiveData == null) {
            growCourseLiveData = new MutableLiveData<>();
        }
        return growCourseLiveData;
    }

    public void getTimeOffList(LifecycleOwner lifecycleOwner) {
        DataRepository.getInstance().requestTimeOffList(lifecycleOwner, timeOffLiveData::postValue);
    }

    public void getSpeedList(LifecycleOwner lifecycleOwner) {
        DataRepository.getInstance().requestSpeedList(lifecycleOwner, speedLiveData::postValue);
    }

    public void getGrowCourse(LifecycleOwner lifecycleOwner, Section section) {
        DataRepository.getInstance().requestPlayerSections(lifecycleOwner, section, growCourseLiveData::postValue);
    }

}
