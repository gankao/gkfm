package com.gankao.gkfm.player;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.util.Log;
import android.widget.SeekBar;

import androidx.lifecycle.MutableLiveData;

import com.aliyun.player.AliPlayer;
import com.aliyun.player.AliPlayerFactory;
import com.aliyun.player.IPlayer;
import com.aliyun.player.bean.ErrorInfo;
import com.aliyun.player.bean.InfoBean;
import com.aliyun.player.bean.InfoCode;
import com.aliyun.player.nativeclass.PlayerConfig;
import com.aliyun.player.source.UrlSource;
import com.gankao.gkfm.data.bean.Section;
import com.gankao.gkfm.utils.DebugLog;

import java.util.Collections;
import java.util.List;

public class PlayerManager {
    public static final int STATE_IDLE = -1;
    public static final int STATE_PREPARED = 2;
    public static final int STATE_PLAYING = 3;
    public static final int STATE_PAUSE = 4;
    public static final int STATE_INTELVAL_PLAYING = 5;
    public static final int STATE_INTELVAL_PAUSE = 6;
    public static final int STATE_COMPLETE = 7;
    public static final int STATE_ERROR = 8;

    public static final int SEQUENCE = 1;
    public static final int RANDOM = 2;

    public boolean seeking = false;


    public MutableLiveData<Integer> STATE = new MutableLiveData();
    private MutableLiveData<Integer> intervalLiveData = new MutableLiveData();
    private MutableLiveData<Integer> repeatTimeLiveData = new MutableLiveData();
    private MutableLiveData<Integer> playOrderLiveData = new MutableLiveData();
    private MutableLiveData<Boolean> autoPlayLiveData = new MutableLiveData();

    private AliPlayer aliyunVodPlayer;
    private boolean isPlaying;
    private MutableLiveData<Integer> mCurrentProgress = new MutableLiveData();
    private MutableLiveData<Integer> mTotalDuration = new MutableLiveData();

    private MutableLiveData<String> mCurrentLetterTime = new MutableLiveData();
    private MutableLiveData<String> mTotalTime = new MutableLiveData();

    private Section currentSection;

    public PlayerManager() {

    }

    public void init(Context context) {
        STATE.setValue(STATE_IDLE);
        intervalLiveData.setValue(2);
        repeatTimeLiveData.setValue(1);
        playOrderLiveData.setValue(SEQUENCE);
        autoPlayLiveData.setValue(false);

        aliyunVodPlayer = AliPlayerFactory.createAliPlayer(context);
        aliyunVodPlayer.enableHardwareDecoder(false);
        //region 阿里云播放器基本设置 具体参考: {@link https://help.aliyun.com/document_detail/124714.html?spm=a2c4g.11186623.6.1083.2dbf2722XQM0Mr}
        //先获取配置
        PlayerConfig config = aliyunVodPlayer.getConfig();
        //设置网络超时时间，单位ms
        config.mNetworkTimeout = 5000;
        //设置超时重试次数。每次重试间隔为networkTimeout。networkRetryCount=0则表示不重试，重试策略app决定，默认值为2
        config.mNetworkRetryCount = 2;
        //  配置请求头 refer UA
        //定义header
//            String[] headers = new String[1];
//            headers[0]="Host:xxx.com";//比如需要设置Host到header中。
//            config.setCustomHeaders(headers);
//            config.mUserAgent = "需要设置的UserAgent";
        //最大延迟。注意：直播有效。当延时比较大时，播放器sdk内部会追帧等，保证播放器的延时在这个范围内。
        config.mMaxDelayTime = 5000;
        // 最大缓冲区时长。单位ms。播放器每次最多加载这么长时间的缓冲数据。
        config.mMaxBufferDuration = 50000;
        //高缓冲时长。单位ms。当网络不好导致加载数据时，如果加载的缓冲时长到达这个值，结束加载状态。
        config.mHighBufferDuration = 3000;
        // 起播缓冲区时长。单位ms。这个时间设置越短，起播越快。也可能会导致播放之后很快就会进入加载状态。
        config.mStartBufferDuration = 500;
        //设置配置给播放器
        aliyunVodPlayer.setConfig(config);
        //endregion

        aliyunVodPlayer.setOnCompletionListener(new IPlayer.OnCompletionListener() {
            @Override
            public void onCompletion() {
                Log.e("test", "=======> onCompletion " + getDuration());
                //播放完成事件
                STATE.setValue(STATE_COMPLETE);
            }
        });
        aliyunVodPlayer.setOnErrorListener(new IPlayer.OnErrorListener() {
            @Override
            public void onError(ErrorInfo errorInfo) {
                //出错事件
                STATE.setValue(STATE_ERROR);
                mCurrentProgress.setValue((int) 0L);
            }
        });
        aliyunVodPlayer.setOnPreparedListener(new IPlayer.OnPreparedListener() {
            @Override
            public void onPrepared() {
                STATE.setValue(STATE_PREPARED);
                mTotalTime.setValue(calculateTime(getDuration() / 1000));
                mTotalDuration.setValue((int) getDuration());
                start();
                //准备成功事件
            }
        });
        aliyunVodPlayer.setOnInfoListener(new IPlayer.OnInfoListener() {
            @Override
            public void onInfo(InfoBean infoBean) {
                if (infoBean.getCode() == InfoCode.AutoPlayStart) {
                    //自动播放开始,需要设置播放状态
                    isPlaying = true;
                } else if (infoBean.getCode() == InfoCode.BufferedPosition) {
                    //更新bufferedPosition
                    long videoBufferedPosition = infoBean.getExtraValue();
                } else if (infoBean.getCode() == InfoCode.CurrentPosition) {
                    //更新currentPosition
                    if (!seeking) {
                        mCurrentLetterTime.setValue(calculateTime(infoBean.getExtraValue() / 1000));
                        mCurrentProgress.setValue((int) infoBean.getExtraValue());
                    }

//                    Log.e("test", "onInfo > CurrentPosition > " + infoBean.getExtraValue());
                } else {

                }
            }
        });
        aliyunVodPlayer.setOnLoadingStatusListener(new IPlayer.OnLoadingStatusListener() {
            @Override
            public void onLoadingBegin() {
                //缓冲开始。
            }

            @Override
            public void onLoadingProgress(int percent, float kbps) {
                //缓冲进度
            }

            @Override
            public void onLoadingEnd() {
                //缓冲结束
            }
        });
        aliyunVodPlayer.setOnStateChangedListener(new IPlayer.OnStateChangedListener() {
            @Override
            public void onStateChanged(int newState) {
                //播放器状态改变事件
            }
        });
        aliyunVodPlayer.setOnSnapShotListener(new IPlayer.OnSnapShotListener() {
            @Override
            public void onSnapShot(Bitmap bm, int with, int height) {
                //截图事件
            }
        });
    }

    public void setAudioUrl(Section currentSection) {
        if (currentSection == null) {
            return;
        }
        STATE.setValue(STATE_IDLE);
        this.currentSection = currentSection;
        mCurrentProgress.setValue((int) 0L);
        mCurrentLetterTime.setValue(calculateTime(0));
        setDataSource(currentSection.src);
    }

    private void setDataSource(String url) {
        UrlSource urlSource = new UrlSource();
        urlSource.setUri(url);
        aliyunVodPlayer.setDataSource(urlSource);
        aliyunVodPlayer.prepare();
    }

    public void togglePlay() {
        if (STATE.getValue() == STATE_PLAYING) {
            pause();
        } else if (STATE.getValue() == STATE_PAUSE) {
            start();
        } else if (STATE.getValue() == STATE_COMPLETE) {
            firstPlay();
        } else if (STATE.getValue() == STATE_IDLE) {
            firstPlay();
        }
    }

    public void start() {
        if (aliyunVodPlayer != null) {
            isPlaying = true;
            aliyunVodPlayer.start();
            STATE.setValue(STATE_PLAYING);
        }
    }

    public void pause() {
        if (aliyunVodPlayer != null) {
            isPlaying = false;
            aliyunVodPlayer.pause();
            STATE.setValue(STATE_PAUSE);
        }
    }

    public void firstPlay() {
        setAudioUrl(currentSection);
    }

    public void stop() {
        if (aliyunVodPlayer != null) {
            isPlaying = false;
            aliyunVodPlayer.stop();
            STATE.setValue(STATE_IDLE);
        }
    }

    public void playNext(Section section) {
        setAudioUrl(section);
    }

    public void playPreious(Section section) {
        setAudioUrl(section);
    }

    public void playAtPosition(Section section) {
        setAudioUrl(section);
    }

    public void reset() {
        if (aliyunVodPlayer != null) {
            isPlaying = false;
            aliyunVodPlayer.reset();
        }
    }

    public void release() {
        if (aliyunVodPlayer != null) {
            isPlaying = false;
            aliyunVodPlayer.release();
        }
    }

    public long getDuration() {
        if (aliyunVodPlayer != null) {
            return aliyunVodPlayer.getDuration();
        }
        return 0L;
    }

    public MutableLiveData<Integer> getCurrentProgress() {
        return this.mCurrentProgress;
    }

    public MutableLiveData<Integer> getTotalDuration() {
        return this.mTotalDuration;
    }

    public MutableLiveData<String> getCurrentLetterTime() {
        return this.mCurrentLetterTime;
    }

    public MutableLiveData<String> getTotalTime() {
        return this.mTotalTime;
    }


    private String calculateTime(long time) {
        long minute;
        long second;
        if (time >= 60) {
            minute = time / 60;
            second = time % 60;
            return (minute < 10 ? "0" + minute : "" + minute) + (second < 10 ? ":0" + second : ":" + second);
        } else {
            second = time;
            if (second < 10) {
                return "00:0" + second;
            }
            return "00:" + second;
        }
    }

    public void startSeek(SeekBar seekBar) {
        seeking = true;
    }

    public void updateSeek(SeekBar seekBar, int progress, boolean fromUser) {
        if (seeking) {
            mCurrentLetterTime.setValue(calculateTime(progress / 1000));
            mCurrentProgress.setValue((int) progress);
        }
    }

    public void seekTo(long time) {
        seeking = false;
        if (aliyunVodPlayer != null)
            aliyunVodPlayer.seekTo(time);
    }

    public void setSpeed(float speed) {
        DebugLog.e("setSpeed: " + speed);
        if (aliyunVodPlayer != null)
            aliyunVodPlayer.setSpeed(speed);
    }

    public void setInterval(int interval) {
        this.intervalLiveData.setValue(interval);
    }

    public void setRepeatTime(int repeatTime) {
        this.repeatTimeLiveData.setValue(repeatTime);
    }

    public void setPlayOrder(int playOrder) {
        this.playOrderLiveData.setValue(playOrder);
    }

    public void setAutoPlay(boolean autoPlay) {
        this.autoPlayLiveData.setValue(autoPlay);
        if (autoPlay) {
            if (STATE.getValue() == STATE_COMPLETE) {
                firstPlay();
            } else if (STATE.getValue() == STATE_IDLE) {
                firstPlay();
            }
        }
    }
}
