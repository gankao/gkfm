package com.gankao.gkfm.http.api;

import com.dragonpower.architecture.http.config.IRequestApi;
import com.dragonpower.architecture.http.config.IRequestType;
import com.dragonpower.architecture.http.model.BodyType;

public final class AnchorListApi implements IRequestApi, IRequestType {

    @Override
    public String getApi() {
        return "fm/fmCats";
    }

    @Override
    public BodyType getType() {
        return BodyType.JSON;
    }

}