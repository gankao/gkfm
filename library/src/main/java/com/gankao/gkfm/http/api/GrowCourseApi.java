package com.gankao.gkfm.http.api;

import com.dragonpower.architecture.http.config.IRequestApi;
import com.dragonpower.architecture.http.config.IRequestType;
import com.dragonpower.architecture.http.model.BodyType;

public final class GrowCourseApi implements IRequestApi, IRequestType {

    @Override
    public String getApi() {
        return "fm/showGrowCourse";
    }

    private String id;

    public IRequestApi setParams(int id) {
        this.id = String.valueOf(id);
        return this;
    }

    @Override
    public BodyType getType() {
        return BodyType.JSON;
    }

}