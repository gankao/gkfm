package com.gankao.gkfm.http.server;


import com.dragonpower.architecture.http.config.IRequestServer;

public class ReleaseServer implements IRequestServer {

    @Override
    public String getHost() {
        return "https://apiv3.gankao.com/";
    }

    @Override
    public String getPath() {
        return "";
    }
}