package com.gankao.gkfm.http.api;

import com.dragonpower.architecture.http.config.IRequestApi;
import com.dragonpower.architecture.http.config.IRequestType;
import com.dragonpower.architecture.http.model.BodyType;

public class ChapterApi implements IRequestApi, IRequestType {

    @Override
    public String getApi() {
        return "course/getResource";
    }

    private String courseId;
    private String sectionId;

    public ChapterApi setParams(String courseId, String sectionId) {
        this.courseId = courseId;
        this.sectionId = sectionId;
        return this;
    }

    @Override
    public BodyType getType() {
        return BodyType.JSON;
    }
}
