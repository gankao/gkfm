package com.gankao.gkfm.http.api;

import com.dragonpower.architecture.http.config.IRequestApi;
import com.dragonpower.architecture.http.config.IRequestType;
import com.dragonpower.architecture.http.model.BodyType;

import java.security.MessageDigest;

public class LoginApi implements IRequestApi, IRequestType {
    @Override
    public String getApi() {
        return "login/sdk";
    }

    //partner_id=debug&mark_id=p123&h5module=kouyu&timestamp=&extra=nickname%3D1%26realname%3D2&sign=39c2ba492321443f8006d057cd0dcbd8
    private String partner_id;
    private String mark_id;
    private String h5module = "";
    private String timestamp = "";
    private String extra = "";
    private String sign = "";

    public LoginApi setLoginInfo(String partner_id, String mark_id, String secret) {
        this.partner_id = partner_id;
        this.mark_id = mark_id;
        this.timestamp = "" + System.currentTimeMillis() / 1000;
        sign = "mark_id=" + mark_id + "&partner_id=" + partner_id + "&timestamp=" + timestamp + secret;
        sign = MD5encode(sign);
        return this;
    }

    public static String MD5encode(String string) {
        byte[] hash = new byte[0];
        try {
            hash = MessageDigest.getInstance("MD5").digest(string.getBytes("UTF-8"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        StringBuilder hex = new StringBuilder(hash.length * 2);
        for (byte b : hash) {
            if ((b & 0xFF) < 0x10) {
                hex.append("0");
            }
            hex.append(Integer.toHexString(b & 0xFF));
        }
        return hex.toString();
    }

    @Override
    public BodyType getType() {
        return BodyType.JSON;
    }
}
