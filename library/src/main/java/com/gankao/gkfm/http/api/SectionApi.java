package com.gankao.gkfm.http.api;

import com.dragonpower.architecture.http.config.IRequestApi;
import com.dragonpower.architecture.http.config.IRequestType;
import com.dragonpower.architecture.http.model.BodyType;

public final class SectionApi implements IRequestApi, IRequestType {

    @Override
    public String getApi() {
        return "fm/fmList";
    }

    private String cat;
    private String anchor;
    private String page;

    public IRequestApi setParams(String cat, int page) {
        this.cat = cat;
        this.anchor = cat;
        this.page = String.valueOf(page);
        return this;
    }

    @Override
    public BodyType getType() {
        return BodyType.JSON;
    }

}