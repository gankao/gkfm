package com.gankao.gkfm.http.server;

public class TestServer extends ReleaseServer {

    @Override
    public String getHost() {
        return "https://apiv3.gankao.com/";
    }
}